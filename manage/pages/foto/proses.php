<center>
<?php
include "koneksi2.php";

$judul_konten = $_POST['judul_konten'];
$ket_foto = $_POST['ket_foto'];
$id_kat = $_POST['id_kat'];
$foto = $_FILES['foto']['name'];
$tmpName = $_FILES['foto']['tmp_name'];
$size = $_FILES['foto']['size'];
$type = $_FILES['foto']['type'];

$maxsize = 9900000000000;
$typeYgBoleh = array("image/jpeg", "image/png", "image/pjpeg");

$dirFoto = "konten";
if (!is_dir($dirFoto))
	mkdir($dirFoto);
$fileTujuanFoto = $dirFoto . "/" . $foto;

$dirThumb = "thumb_konten";
if (!is_dir($dirThumb))
	mkdir($dirThumb);
$fileTujuanThumb = $dirThumb . "/t_" . $foto;

$dataValid = "YA";

if ($size > 0) {
	if ($size > $maxsize) {
		echo "Ukuran File Terlalu besar! <br/>";
		$dataValid = "TIDAK";
	}
	if (!in_array($type, $typeYgBoleh)) {
		echo "Maaf, Kami tidak mengenal file tersebut! <br/>";
		$dataValid = "TIDAK";
	}
}

if (strlen(trim($judul_konten)) == 0) {
	echo "Masih Ada Kesalahan. Silahkan diulangi kembali. <br />";
	$dataValid = "TIDAK";
}
if (strlen(trim($ket_foto)) == 0) {
	echo "Masih Ada Kesalahan. Silahkan Diulangi kembali. <br />";
	$dataValid = "TIDAK";
}
if ($dataValid == "TIDAK") {
	echo "Masih Ada Kesalahan, silakan perbaiki! </br>";
	echo "<input type='button' value='Kembali'
		onClick='self.history.back()'>";
	exit;
}

$sql = "INSERT INTO content (judul_konten, ket_foto, id_kat, foto) VALUES ('$judul_konten','$ket_foto','$id_kat','$foto')";
$hasil = mysqli_query($kon, $sql);

if ($size > 0) {
	if (!move_uploaded_file($tmpName, $fileTujuanFoto)) {
		echo "Gagal Upload Gambar! <br/>";
		echo "<a href='?page=tambah_soal'>Kembali ke halaman input soal.</a>";
		exit;
	} else {
		buat_thumbnail($fileTujuanFoto, $fileTujuanThumb);
	}
}
echo "<br/>File Sudah Diupload! <br/>";

function buat_thumbnail($file_src, $file_dst)
{
	list($w_src, $h_src, $type) = getImageSize($file_src);

	switch ($type) {
		case 1: // gif -> jpg
			$img_src = imagecreatefromgif($file_src);
			break;
		case 2: // jpeg -> jpg
			$img_src = imagecreatefromjpeg($file_src);
			break;
		case 3: // png -> jpg
			$img_src = imagecreatefrompng($file_src);
			break;
	}

	$thumb = 100;
	if ($w_src > $h_src) {
		$w_dst = $thumb;
		$h_dst = round($thumb / $w_src * $h_src);
	} else {
		$w_dst = round($thumb / $w_src * $h_src);
		$h_dst = $thumb;
	}

	$img_dst = imagecreatetruecolor($w_dst, $h_dst);

	imagecopyresampled(
		$img_dst,
		$img_src,
		0,
		0,
		0,
		0,
		$w_dst,
		$h_dst,
		$w_src,
		$h_src
	);
	imagejpeg($img_dst, $file_dst);
	imagedestroy($img_src);
	imagedestroy($img_dst);
}

echo "<br/>File Sudah Diupload! <br/>";

if ($hasil){
?>
<script language="JavaScript">
            alert('Anda Berhasil Menambah Konten');
            window.location='../../';
        </script>
		<?php
}
		?>
</center>