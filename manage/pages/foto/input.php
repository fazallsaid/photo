<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                       <h2>Inputkan Karya Anda</h2>
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
				<form action="pages/foto/proses3.php" method="POST" enctype="multipart/form-data">
				<div class="form-group">
				<label>Judul Karya</label>
				<input type="text" name="judul_konten" placeholder="masukkan judul Anda" class="form-control">
				</div>
				<div class="form-group">
				<label>Keterangan Karya</label>
				<textarea name="ket_foto" placeholder="masukkan keterangan Anda" class="form-control" rows="15"></textarea>
				</div>
				<div class="form-group">
				<label>Kategori Karya</label>
				<select name="id_kat" class="form-control">
				<option value="0">Pilih Kategori</option>
				<?php
						$quua = mysqli_query($connection, "SELECT * FROM category");
						while ($fot = mysqli_fetch_array($quua)):
						?>
				<option value="<?php echo $fot['id_kat']; ?>"><?php echo $fot['nama_kat']; ?></option>
				<?php endwhile; ?>
				</select>
				</div>
				<div class="form-group">
				<label>Foto Karya</label>
				<input type="file" name="foto" class="form-control">
				</div>
				<div class="form-group">
				<input type="submit" class="btn btn-success" name="upload" value="Post!">
				</div>
				</form>
				
                 <!-- /. ROW  -->           
    </div>
             <!-- /. PAGE INNER  -->
            </div>