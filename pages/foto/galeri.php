<section class="gallery-section">
		<div class="row gallery-gird">
		<?php
		$as = mysqli_query($connection, "SELECT * FROM content JOIN category ON content.id_kat=category.id_kat ORDER BY id_konten DESC");
		while ($asu = mysqli_fetch_array($as)):
		?>
			<div class="col-lg-4 col-xl-3 col-sm-6">
				<a class="gallery-item fresco" href="manage/pages/foto/foto_konten/<?php echo $asu['foto']; ?>" data-fresco-group="projects">
					<img src="manage/pages/foto/foto_konten/<?php echo $asu['foto']; ?>" alt="">
					<div class="gi-text">
						<h4><?php echo $asu['judul_konten']; ?></h4>
						<p><?php echo $asu['nama_kat']; ?></p>
					</div>
				</a>
			</div>
			<?php endwhile; ?>
		</div>
	</section>