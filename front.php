<section class="hero-section">
		<div class="hero-slider owl-carousel">
		<?php
		$as = mysqli_query($connection, "SELECT * FROM content JOIN category ON content.id_kat=category.id_kat ORDER BY id_konten DESC LIMIT 3");
		while ($asu = mysqli_fetch_array($as)):
		?>
			<div class="hero-item">
				<div class="hero-text">
					<div class="ht-cata"><?php echo $asu['nama_kat']; ?></div>
					<h2><?php echo $asu['judul_konten']; ?></h2>
					<p><?php echo $asu['ket_foto']; ?></p>
					<a href="?page=galeri" class="ht-btn">Lihat lebih lanjut <i class="arrow_right"></i></a>
				</div>
				<div class="hi-bg set-bg" data-setbg="manage/pages/foto/foto_konten/<?php echo $asu['foto']; ?>"></div>
			</div>
			<?php endwhile; ?>
		</div>
	</section>